<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?= base_url() ?>css/style.css?v1.1">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    <link rel="shortcut icon" type="image/png" href="<?= base_url() ?>img/logo.png"/>
    <title>Aduan</title>
</head>
<body>
<div class="wrapper rounded d-flex align-items-stretch">
    <div class="bg-yellow">
        <div>
            <img class="img-thumbnail" src="<?= base_url() ?>img/logo.png" alt="">
        </div>
        <div class="pt-5 cursive"> Sila pilih jenis aduan serta nyatakan aduan anda </div>
        <div class="pt-sm-5 pt-5 cursive mt-sm-5"> Kami memerlukan nombor telefon anda supaya kami dapat menghubungi anda semula </div>
    </div>
    <div class="contact-form">
        <div class="h3">Aduan</div>
        <form action="<?= base_url() ?>aduan" method="post">
        <div class="form-group pt-3">
            <label for="message">Sila plih jenis aduan</label>
            <select class="form-control" name="type" id="">
                <option value="Maklumat Pembelian / Penghantaran">Maklumat Pembelian / Penghantaran</option>
                <option value="Pengesahan Pesanan">Pengesahan Pesanan</option>
                <option value="Teknikal">Teknikal</option>
                <option value="Pembayaran" selected="selected">Pembayaran</option>
                <option value="Pakej / Kuantiti">Pakej / Kuantiti</option>
                <option value="Tracking dan Penghantaran">Tracking &amp; Penghantaran</option>
                <option value="Produk">Produk</option>
                <option value="Cadangan Penambahbaikkan">Cadangan Penambahbaikkan</option>
                <option value="Pembelian Semula">Pembelian Semula</option>
            </select>
        </div>
            <div class="form-group pt-3"> <label for="message">Aduan anda</label> <textarea name="message" class="form-control" required></textarea> </div>
            <div class="d-flex align-items-center flex-wrap justify-content-between pt-4">
                <div class="form-group pt-lg-2 pt-3"> <label for="name">Nama</label> <input type="text" name="name" class="form-control" required> </div>
                <div class="form-group pt-lg-2 pt-3"> <label for="name">No. Telefon</label> <input type="text" name="phone" class="form-control" required> </div>
            </div>
            <div class="d-flex align-items-center flex-wrap justify-content-between pt-lg-5 mt-lg-4 mt-5">
                <div class="btn btn-default"> Batal </div>
                <button class="btn btn-primary" type="submit"> Hantar Aduan</button>
            </div>
        </form>
    </div>
</div> 
</body>
</html>

<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>

<?php 
if ($this->session->flashdata('modal')) {
    echo '<script>$("#myModal").modal("show")</script>';
}
?>