<?php

function filter_phone_number($phone_num = ''){

    $phone_num = str_replace( array( '\'', '"', ',' , ';', '<', '>', '-', '+', '.', ' ' ), '', $phone_num);
    $first_char = substr($phone_num,0,1);
    $second_char = substr($phone_num,1,1);
    $string_length = strlen($phone_num);
    if($string_length > 1){
        if($first_char == '6' && $second_char == '0'){
            //do nothing
        } else if($first_char == '0'){
            $phone_num = '6'.$phone_num;
        } 
    }
    
    return trim($phone_num);
}