<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aduan extends CI_Controller {

	function __construct(){
        parent::__construct();
		$this->load->model('aduan_m');
    }

	public function index()
	{
		$this->aduan_m->insert_aduan();
		$this->load->view('aduan_v');
	}

	public function thank_you(){
		$this->load->view('thank_you_v');
	}
}
