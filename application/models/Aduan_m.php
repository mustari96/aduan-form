<?php 

class Aduan_m extends CI_Model {

    public function insert_aduan(){
        if ($_POST) {
            $data['complaint_name'] = $this->input->post('name');
            $data['complaint_phone'] = filter_phone_number($this->input->post('phone'));
            $data['complaint_type'] = $this->input->post('type');
            $data['complaint_message'] = $this->input->post('message');
            $data['dt_added'] = date('Y-m-d H:i:s');

            $this->db->insert('complaint_list',$data);

            $this->session->set_flashdata('modal','success');
            return redirect('aduan/thank_you');
        }
    }
}